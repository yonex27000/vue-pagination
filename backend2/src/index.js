const express = require('express')
const cors = require('cors')
const app = express()


require('./database')
app.use(cors())
app.use('/', require('./routes/index'))

app.listen(3000,()=>{
    console.log("SERVER ON PORT 3000")
})