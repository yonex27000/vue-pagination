const {Schema, model} = require('mongoose')
const articleSchema = new Schema({
    title: String,
    description: String,
    imageUrl: String
})

module.exports = model('Article', articleSchema)