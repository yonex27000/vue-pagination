const {Router} = require('express')
const faker = require('faker')
const Article = require('../models/Article')
const router = Router()

router.get('/create', async (req,res)=>{
    for(let i=0; i<100; i++){
        await Article.create({
            title: faker.name.title(),
            description: faker.lorem.paragraph(),
            imageUrl:faker.image.imageUrl()
        })
    }
    res.send('100 records created');
})
router.get('/articles', async(req,res)=>{
    const articles = await Article.find()
    res.json(articles);
})
module.exports = router